package app
import (
	"github.com/gin-gonic/gin"
	"gitlab.com/irmitcod/bookstore-users-api/logger"
)

var (
	router = gin.Default()
)

/**
this funn start app with map url
 */
func StartApplication() {
	//load map url from same package
	MapUrl()
	//show log
	logger.Info("Start application")
	//run server on port 8081
	router.Run(":8081")
}
