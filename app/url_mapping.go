package app

import (
	"gitlab.com/irmitcod/gotest/controllers/ping"
	"gitlab.com/irmitcod/gotest/controllers/user"
)

//create map url with gin
func MapUrl() {
	//test server
	router.GET("/ping", ping.Ping)
	//rout for create
	router.POST("/users", user.Create)

}
