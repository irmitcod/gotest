package user

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"gitlab.com/irmitcod/gotest/utils"
	"gitlab.com/irmitcod/gotest/utils/errors"
	"math/rand"

	"gitlab.com/irmitcod/gotest/domain/users"

	"net/http"
)

func Create(c *gin.Context) {
	//create user
	var user users.User

	//check for json if json has error show err
	if err := c.ShouldBindJSON(&user); err != nil {
		restError := errors.NewBadRequest("Invalid Json")
		c.JSON(http.StatusOK, restError)
		return
	}

	//change user info with random straing
	user.Name = fmt.Sprintf("%s %s", user.Name, utils.String(5))
	//change user age wiht random int
	user.Age += rand.Intn(30)
	//change active
	user.Active = utils.RandBool()
	//return standard response to users
	c.JSON(http.StatusOK, user)

}
