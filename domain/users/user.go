package users

type User struct {
	Name   string `json:"name"`
	Age    int    `json:"age"`
	Active bool    `json:"active"`
}
