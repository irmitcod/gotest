package response

type RestResponse struct {
	Status int `json:"status"`
	Result interface{} `json:"result"`
}

func GetResponse(items interface{}) *RestResponse {
	return &RestResponse{
		Status: 1,
		Result: items,
	}
}