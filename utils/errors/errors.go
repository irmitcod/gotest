package errors

import (
	"errors"
	"net/http"
)

type RestError struct {
	Message string `json:"message"`
	Status  int    `json:"status"`
	Error   string `json:"error"`
}
func NewError(msg string) error {
	return errors.New(msg)
}

func NewBadRequest(message string) *RestError {
	return &RestError{
		Message: message,
		Status:  http.StatusBadRequest,
	}
}
func NewNotFound(message string) *RestError {
	return &RestError{
		Message: message,
		Status:  http.StatusNotFound,
	}
}

func NewNinternalServerError(message string) *RestError {
	return &RestError{
		Message: message,
		Status:  http.StatusInternalServerError,
		Error:   "Status InternalServer Error",
	}
}
